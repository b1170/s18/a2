let courses = [

    {
        id: "1",
        name: "Python 101",
        description: "Learn the basics of python.",
        price: 15000,
        isActive: true

    }, {
        id: "2",
        name: "CSS 101",
        description: "Learn the basics of CSS.",
        price: 10500,
        isActive: true

    }, {
        id: "3",
        name: "CSS 102",
        description: "Learn an advanced CSS.",
        price: 15000,
        isActive: false

    }, {
        id: "4",
        name: "PHP-Laravel 101",
        description: "Learn the basics of PHP and its Laravel framework.",
        price: 20000,
        isActive: true

    }
];

/* //Create
-Create an arrow function called addCourse which allows us to add a new object into the array. This function should receive the following the data:
-id,
-name,
-description,
-price
-isActive
-This function should be able to show an alert after adding into the array:
"You have created <nameOfCourse>. Its price is <priceOfCourse>"
-push method (to push an object in the array of objects) */


let newCourse = (id, name, description, price, isActive) => {

    courses.push({
        id,
        name,
        description,
        price,
        isActive
    })
    console.log(`You have created ${name}. Its price is ${price}`)

};

newCourse("5", "NewCourse", "This is a new course", 2000, true);


/* 	-Create an arrow function getSingleCourse which allows us to find a particular course by the providing the course's id.

			-Show the details of the found course in the console.
			-return the found course
			- find method

		-Create an arrow function getAllCourses which is able to show all of the items/objects in the array in our console.

			-return the courses array
			- any method (so long as you can return all the courses) */


let getSingleCourse = (id) => {
    for (let i = 0; i < courses.length; i++) {
        if (courses[i].id == id) {
            console.log(courses[i]);
        }
    }
};
getSingleCourse(5);

let getAllCourses = (x) => {
    for (let i = 0; i < courses.length; i++) {
        console.log(courses[i]);
    }
};
getAllCourses();


/* //Update

-Create an arrow function called archiveCourse which is able to update/re-assign the isActive property of a particular course, update the isActive property to false. This function should be able to receive the particular index number of the item as an argument.

-show the updated course in the console.*/

// let archiveCOurse = (id, isActive) => {
//     if (id == courses.id) {
//         let courses.isActive[i]
//     }
// }

let archiveCourse = (id, isActive) => {
    for (let i = 0; i < courses.length; i++) {
        if (courses[i].id == id) {
            courses[i].isActive = isActive;
        }
    }
};

archiveCourse(3, false);
console.log(courses);


/* //Delete
-Create an arrow function called deleteCourse which is able to delete the last course object in the array.
-pop method


Stretch Goals (Added challenge only/No need to accomplish both):

Create an arrow function which can show all of the active courses only. Show the active courses in the console. See .filter()
- you may store the result of filter method in a variable

In the archiveCourse function, the function should instead receive the name of the course and use the name to find the index number of the course.  See findIndex()
- - you may store the result of filter method in a variable */

let deleteCourse = courses.pop();
console.log(deleteCourse);
